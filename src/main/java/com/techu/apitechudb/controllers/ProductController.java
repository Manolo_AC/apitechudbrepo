package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by U501257 on 26/05/2021.
 */

@RestController
@RequestMapping("/apitechu/v2")
// @CrossOrigin(origins = "*", methods ={RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT})
// Puede ponerse a nivel general o en cada petición.

public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    @CrossOrigin(origins = "*", methods ={RequestMethod.GET})
    //Soluciona errores del front Cross not allowed. Esto no era necesario para postman. Necesario en cada función.

    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");

        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("addProduct");
        System.out.println("La id del producto que se va a crear es " + product.getId());
        System.out.println("La descripción del producto que se va a crear es " + product.getDesc());
        System.out.println("El precio del producto que se va a crear es " + product.getPrice());

        return new ResponseEntity<>(this.productService.add(product), HttpStatus.CREATED);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es " + id);


        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
// Esto es un if then else reducido
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel productModel, @PathVariable String id){
        System.out.println("updateProiduct");
        System.out.println("La id del producto que se va a actualizar en parámetro es " + id);
        System.out.println("La id del producto que se va a actualizar es " + productModel.getId());
        System.out.println("La descripción del producto que se va a actualizar es " + productModel.getDesc());
        System.out.println("El precio del producto que se va a actualizar es " + productModel.getPrice());

        Optional<ProductModel> productToUpdate = this.productService.findById(id);

        if (productToUpdate. isPresent()) {
            System.out.println("Producto para actualizar encontrado, actualizando");
            this.productService.update(productModel);
        }

        return new ResponseEntity<>(productModel,
                productToUpdate.isPresent() ?  HttpStatus.OK : HttpStatus.NOT_FOUND);

    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es " + id);

        boolean deletedProduct = this.productService.delete(id);

        return  new ResponseEntity<>(
                deletedProduct ? "Producto borrado" : "Producto no borrado",
             // la respuesta es un string, hay que declararlo así en ResponseEntity<String>
                deletedProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
//operador ternario == If/Then/Else
    }
}
