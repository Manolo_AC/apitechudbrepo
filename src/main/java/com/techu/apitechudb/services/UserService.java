package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by U501257 on 02/06/2021.
 */


@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> getUsers(String orderBy){
        System.out.println ("getUsers");

        List <UserModel> result;

        if (orderBy != null) {
            result = this.userRepository.findAll(Sort.by("age"));
        } else {
            result = this.userRepository.findAll();
        }

        return result;

    }

// Añado código para ordenar por edad de mode ascendente
//    public List<UserModel> findAllAgeSorted(){
//        return this.userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));
//    }

    public UserModel addUser(UserModel user) {
        System.out.println("addUser");

        return this.userRepository.save(user);
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById");
        System.out.println("La id del usuario a buscar es " + id);

        return this.userRepository.findById(id);
    }

    public UserModel update(UserModel user) {
        System.out.println("update");

        return this.userRepository.save(user);
    }

    public boolean delete(String id) {
        System.out.println("delete");

        boolean result =false;

        if (this.userRepository.findById(id).isPresent() == true) {
            System.out.println("Usuario encontrado, borrando");
            this.userRepository.deleteById(id);

            result = true;
        }


        return result;

    }
}
