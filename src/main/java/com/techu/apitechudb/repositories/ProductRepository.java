package com.techu.apitechudb.repositories;

import com.techu.apitechudb.models.ProductModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by U501257 on 31/05/2021.
 */
@Repository
public interface ProductRepository extends MongoRepository<ProductModel, String>{
}
