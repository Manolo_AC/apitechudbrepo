package com.techu.apitechudb.repositories;

import com.techu.apitechudb.models.PurchaseModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by U501257 on 09/06/2021.
 */
@Repository
public interface PurchaseRepository extends MongoRepository<PurchaseModel, String> {
}


