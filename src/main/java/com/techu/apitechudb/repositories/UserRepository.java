package com.techu.apitechudb.repositories;

import com.techu.apitechudb.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by U501257 on 02/06/2021.
 */

@Repository
public interface UserRepository extends MongoRepository<UserModel, String> {
}